package bsa.java.concurrency.fs;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class FileService implements FileSystem{

    @Value("${files.storage}")
    private String filesStorage;

    @Async
    @Override
    public CompletableFuture<String> save(byte[] img) {
        if (!Files.exists(Path.of(filesStorage))) {
            new File(filesStorage).mkdirs();
        }

        UUID fileName = UUID.randomUUID();
        try {
            BufferedImage image = ImageIO.read(new ByteArrayInputStream(img));
            ImageIO.write(image, "jpg", new File(filesStorage + fileName + ".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(fileName.toString());
    }

    @Async
    @Override
    public void delete(UUID id) {
        new File(filesStorage + id + ".jpg").delete();
    }

    @Async
    @Override
    public void clearAll() {
        try {
            FileUtils.cleanDirectory(new File(filesStorage));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
