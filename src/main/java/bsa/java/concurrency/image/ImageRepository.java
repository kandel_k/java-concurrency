package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ImageRepository extends JpaRepository<ImageEntity, UUID> {

    @Query(value = "SELECT cast(i.id as varchar) AS imageId, round(1.0 - CAST(bitcount(i.hash # :hash) AS numeric) / 64, 4) as matchPercent, i.url as imageUrl " +
            "FROM images i " +
            "WHERE i.id IN ( " +
            "SELECT i2.id " +
            "FROM images i2 " +
            "WHERE round(1.0 - CAST(bitcount(i2.hash # :hash) AS numeric) / 64, 4) >= :threshold)",
            nativeQuery = true)
    List<SearchResultDTO> findClosestByHashAndThreshold(@Param("hash") long hash, @Param("threshold") double threshold);

    @Query(value = "SELECT url FROM images WHERE id = :id",
            nativeQuery = true)
    String findUrlById(@Param("id") UUID id);
}
