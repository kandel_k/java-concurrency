package bsa.java.concurrency.image;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "images")
public class ImageEntity {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private Long hash;

    @Column
    private String url;

    public ImageEntity(Long hash, String fileName) {
        this.id = UUID.fromString(fileName);
        this.hash = hash;
        this.url = "http://127.0.0.1:8080/files/" + fileName + ".jpg";
    }
}
