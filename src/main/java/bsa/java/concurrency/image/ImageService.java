package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.util.DHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class ImageService {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private FileSystem fileService;

    public void uploadImages(MultipartFile[] files) {
        Arrays.asList(files).parallelStream().forEach(file -> {
            try {
                var fileBytes = file.getBytes();
                var filename = fileService.save(fileBytes);
                var hash = DHash.countHash(fileBytes);
                imageRepository.save(new ImageEntity(hash, filename.get()));
            } catch (IOException | ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    public List<SearchResultDTO> searchImages(MultipartFile file, double threshold) throws IOException {
        var fileBytes = file.getBytes();
        var hash = DHash.countHash(fileBytes);
        var foundImages = imageRepository.findClosestByHashAndThreshold(hash, threshold);

        if (foundImages.isEmpty()) {
            CompletableFuture.runAsync(() -> {
                try {
                    var fileName = fileService.save(fileBytes);
                    imageRepository.save(new ImageEntity(hash, fileName.get()));
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
        }
        return foundImages;
    }

    public void deleteImage(UUID imageId) {
        fileService.delete(imageId);
        imageRepository.deleteById(imageId);
    }

    public void clearAll() {
        imageRepository.deleteAll();
        fileService.clearAll();
    }
}
