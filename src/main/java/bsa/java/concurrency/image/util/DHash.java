package bsa.java.concurrency.image.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class DHash {

    public static long countHash(byte[] imgBytes) {
        try {
            BufferedImage image = ImageIO.read(new ByteArrayInputStream(imgBytes));
            return countDHash(preprocessImage(image));
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private static BufferedImage preprocessImage(BufferedImage img) {
        Image scaledImage = img.getScaledInstance(9, 8, Image.SCALE_SMOOTH);
        BufferedImage devaluedImage = new BufferedImage(9, 8, BufferedImage.TYPE_BYTE_GRAY);
        devaluedImage.getGraphics().drawImage(scaledImage, 0, 0, null);

        return devaluedImage;
    }

    private static long countDHash(BufferedImage img) {
        long hash = 0, curr, prev;
        for (int row = 1; row < 8; row++) {
            for (int col = 1; col < 9; col++) {

                prev = img.getRGB(col - 1, row - 1);
                curr = img.getRGB(col, row);
                hash |= curr > prev ? 1 : 0;
                hash <<= 1;
            }
        }
        return hash;
    }
}
